/**
 * Counter.java
 */

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;

/**
 * Program to count frequency of alphabets in a File
 * 
 * @author Jaydeep Untwal, Shubham Saxena
 */

public class Counter {

	/**
	 * To print frequency of alphabets
	 * 
	 * @param counter 
	 */
    public static void printCounter(int counter[]){
		for (int index = 'A'; index <= 'Z'; index ++){
			if(counter[index]!=0)
				System.out.println((char)index + ":	" + counter[index]);
		}
		for (int index = 'a';index <= 'z';index ++){
			if(counter[index]!=0)	
				System.out.println((char)index + ":	" + counter[index]);
		}
    }
    
    /**
     * To print frequency of alphabets
     * 
     * @param counter Array to store frequency
     * @param pos Array of unique alphabets present in File
     */
    public static void printCounterOpt( int counter[],int pos[]){
		for(int i=0;i<pos.length;i++){
				if(counter[pos[i]]!=0){
				System.out.println((char) pos[i] + ":	" + counter[pos[i]]);
				}
		}
    }
    
	/**
	 * Function to count character frequency
	 * 
	 * @param aInputHandle Scanner passed by main
	 */
    public static void x(Scanner aInputHandle){
		
    	int alpha[] = new int[123]; // Array of Alphabets
    	
		while (aInputHandle.hasNextLine()){
			
			String s = aInputHandle.nextLine();
			
			for(int i=0;i<s.length();i++){
				int n = s.charAt(i);
				alpha[n]++;
			}
		}
		
		printCounter(alpha); // Call print function
		
    } // End of x
    
	/**
	 * Optimized function to count character frequency
	 * 
	 * @param aInputHandle Scanner passed by main
	 */
    public static void y(Scanner aInputHandle){
		
		int alpha[] = new int[123]; // Array of Alphabets
		int j=0; // index of pos
		int pos[] = new int[52]; // Array of required alphabets
		
		while (aInputHandle.hasNextLine()){

			String s = aInputHandle.nextLine();

			for(int i=0;i<s.length();i++){
				int n = s.charAt(i);
				alpha[n]++;
				
				if(alpha[n]==1){
					pos[j]=n;
					j++;
				}
			} // End of For
		} // End of While
		
		printCounterOpt(alpha,pos); // Call print function
    } // End of CountOpt

/**
 * Call scanner handlers and compute execution time
 * 
 * @param args File which contains characters
 */
    public static void main(String[] args) {
    	
	   	for (String arg: args) { // For all input files
			try {
				Scanner scan;
				long startTime = System.currentTimeMillis(); //current time
				x(scan = new Scanner(new File(arg)));
				long endTime = System.currentTimeMillis();
				endTime = endTime - startTime;
				System.out.println("Time taken(Unoptimized): "+endTime+" ms");
				
				System.out.println("======================================\n");
				
				startTime = System.currentTimeMillis();
				y(scan = new Scanner(new File(arg)));
				endTime = System.currentTimeMillis();
				endTime = endTime - startTime;
				System.out.println("Time taken(Optimized): "+endTime+" ms");
				System.out.println("========================================");
				
				scan.close(); // For security reasons
	
			} catch (FileNotFoundException e){	
				System.err.println("File not found.");
				System.exit(0); // Quit as file not available
			} catch (Exception e){	
				System.err.println("Something wrong");
				System.exit(1); // Quit with error in code
			}
		} // End of For
    } // End of Main
} // End of Class
