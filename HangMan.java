/**
 * Hangman.java
 */

import java.io.*;
import java.util.Scanner;

/**
 * A program to play the game 'Hangman'
 * Rules of the game:
 * You get 9 chances to guess the given word
 * You can only guess one letter at a time
 * You can type '1' to pass current word
 * 
 * @author Jaydeep Untwal, Shubham Saxena
 *
 */
public class HangMan {
	
	String gallow[] = new String[17]; // String array to store the gallow
	
	/**
	 * To play for current word
	 * 
	 * @param wordC Array to store word character by character
	 * @param guessed Array to store currently guessed letters
	 */
	public void playForWord(char wordC[],char guessed[]){
		
		Scanner scan = new Scanner(System.in);
		// Array to store letters already used for current play
		char[] used = {'0','0','0','0','0','0','0','0','0'};
		int used_index = 0; // index of array 'used'
		int win=0; // Counter to check win
		int lose=0; // Counter to check lose
		
		while(lose<=8 && win != wordC.length){
					
			int flag = 0; 
			
			// Print already used letters
			System.out.print("Wrong Guesses: ");
			for(int i=0;i<used.length;i++){
				if(used[i]!='0'){
					System.out.print(used[i]+",");
				}
			}
			
			System.out.println("");
			
			System.out.print(lose+": ");
			System.out.println(guessed);
			int flag_used = 0; // Flag to check if letter is already used
			
			String input = scan.next();
			char in = input.charAt(0);
			in = Character.toLowerCase(in); // Convert letter to lowercase
			
			// Check if letter was already used
			for(int i=0;i<9;i++){
				if(used[i] == in){
					flag_used = 1;
				}
			}
			
			if(in!='1'){ // Check if user has not passed the chance
			
				for(int j=0;j<wordC.length;j++){
					// If correctly guessed letter was not used
					if(wordC[j]==in && guessed[j]!=in){
						guessed[j]=in;
						flag = 1;
						win++;
					}
					
					// If letter was already used
					if(guessed[j]==in){
						flag=2;
					}
					
				}
						
				// On wrong guess
				if(flag==0 && flag_used==0){
					
					// Put letter in 'used'
					used[used_index] = in;
					used_index++;
					
					// Increment lose
					lose++;
						
					// Print corresponding gallow
					switch(lose){
					case 1:
						int c1[] = {16,16,16,16,16,16,16,16,
								-1,9,10,11,-1,-1,-1,-1,-1};
						printGallow(c1);	
						break;
					case 2:
						int c2[] = {12,12,12,12,12,12,12,12,
								-1,9,10,11,-1,-1,-1,-1,-1};
						printGallow(c2);	
						break;
					case 3:
						int c3[] = {0,12,12,12,12,12,12,12,
								-1,9,10,11,-1,-1,-1,-1,-1};
						printGallow(c3);	
						break;
					case 4:
						int c4[] = {0,15,14,13,12,12,12,12,
								-1,9,10,11,-1,-1,-1,-1,-1};
						printGallow(c4);
						System.out.println("C'mon, it's not that difficult\n");
						break;
					case 5:
						int c5[] = {0,1,2,13,12,12,12,12,
								-1,9,10,11,-1,-1,-1,-1,-1};
						printGallow(c5);	
						break;
					case 6:
						int c6[] = {0,1,2,3,4,12,12,12,
								-1,9,10,11,-1,-1,-1,-1,-1};
						printGallow(c6);	
						break;
					case 7:
						int c7[] = {0,1,2,3,4,5,5,5,-1,9,
								10,11,-1,-1,-1,-1,-1};
						printGallow(c7);	
						break;
					case 8:
						int c8[] = {0,1,2,3,4,5,6,5,-1,9,
								10,11,-1,-1,-1,-1,-1};
						printGallow(c8);	
						System.out.println("Last Chance ! Be Careful\n");
						break;
					case 9:
						int c9[] = {0,1,2,3,4,5,6,5,-1,8,
								10,11,-1,-1,-1,-1,-1};
						printGallow(c9);	
						break;
					}
				}
			} else {
				lose = 9; // If user inputs '1' (Give up)
			}

		} // End of While
		
		// When user loses the current play
		if(lose==9){
			System.out.print("You Lose! The word was '");
			
			for(int i=0;i<wordC.length;i++){
				System.out.print(wordC[i]);
			}
			System.out.print("', Try the next word!\n");
			System.out.println("");
			
			lose = 0;
			win = 0;
		}
		
		// When user correctly guesses the word
		if(win==wordC.length){
			System.out.print(lose+": ");
			System.out.println(guessed);
			System.out.println("Correct! You guessed the word,"
					+ " try the next word :)");
			win = 0;
			lose = 0;
		}

		
	} // End of playForWord

	/**
	 * Get each word from input and call playForWord
	 * 
	 * @param f input file passed by main
	 * @throws FileNotFoundException
	 */
	public void play(File f) throws FileNotFoundException{
		
		Scanner scan = new Scanner(f); // Scanner to read file
		
		while(scan.hasNextLine()){
			String word = scan.nextLine(); // Save word as String
			// Array to store word character by character
			char wordC[] = new char[word.length()];
			// Array to store correct guessed letters out of the word
			char guessed[] = new char[word.length()];
			
			// Convert word to lowerCase
			for(int i=0;i<word.length();i++){
				wordC[i] = Character.toLowerCase(word.charAt(i));
				guessed[i] = '-';
			}
			
			playForWord(wordC,guessed); // play for current word
		} // End of while
		
		System.out.println("End of Game, Thank you for playing HangMan :)");
		
		scan.close(); // For Security reasons
		
	} // End of play
	
	
	/**
	 * Function to print gallow
	 * 
	 * @param n Array to check if particular string needs to be printed
	 */
	public void printGallow(int n[]){
		for(int i=0;i<n.length;i++){
			if(n[i]>=0){
				System.out.println(gallow[n[i]]);
			}
		}
		System.out.println("");
	} // End of printGallow
	
	/**
	 * @param args[0] filename which contains words
	 */
	public static void main(String[] args) {
		
		HangMan hm = new HangMan(); // Object of class HangMan
		
		System.out.println("WELCOME TO THE WORLD OF HANGMAN !!!\n");
		
		hm.gallow[0] = "     #############";
		hm.gallow[1] = "     #  #        #";
		hm.gallow[2] = "     # #         #";
		hm.gallow[3] = "     ##        #####";
		hm.gallow[4] = "     #         #####";
		hm.gallow[5] = "     #           #";
		hm.gallow[6] = "     #          ###";
		hm.gallow[7] = "     #           #";
		hm.gallow[8] = "  #########     # #";
		hm.gallow[9] = "  #########";
		hm.gallow[10] = " ##       ##";
		hm.gallow[11] = "###       ###";
		hm.gallow[12] = "     #";
		hm.gallow[13] = "     ##";
		hm.gallow[14] = "     # #";
		hm.gallow[15] = "     #  #";
		hm.gallow[16] = "";
	
		// Order of print (-1 to not print)
		int n[] = {0,1,2,3,4,5,6,7,8,-1,10,11,-1,-1,-1,-1,-1};
		hm.printGallow(n); // Call Print function
			
		System.out.println("\nHere is your first word... Enjoy !!!\n");
		
		try{
		File wordTxt = new File(args[0]);
		hm.play(wordTxt);
		}
		
		catch (Exception e){
			System.out.println("File not found, "
					+ "Please check if the file exists");
			System.exit(1);
		}
		
	} // End of Main

} // End of Class